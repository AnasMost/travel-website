import throttle from 'lodash/throttle'
import debounce from 'lodash/debounce'

class RevealOnScroll {
    constructor(elems, thresholdRatio) {
        this.thresholdRatio = thresholdRatio
        this.itemsToReveal = elems
        this.browserHeight = window.innerHeight
        this.hideInitially()
        this.scrollThrottle = throttle(this.calcCaller, 200).bind(this)
        this.events()
    }

    events() {
        window.addEventListener("scroll", this.scrollThrottle)
        window.addEventListener("resize", debounce(
            () => this.browserHeight = window.innerHeight, 444)
        )
    }

    calcCaller() {
        this.itemsToReveal.forEach(elem => {
            if(elem.isRevealed == false) {
                this.calculateIfScrolledTo(elem)
            }
        })
    }

    calculateIfScrolledTo(elem) {
        let browserBottom = window.scrollY + this.browserHeight
        if(browserBottom > elem.offsetTop) {
            let scrollRatio = elem.getBoundingClientRect().top / this.browserHeight
            if (scrollRatio < this.thresholdRatio) {
                elem.classList.add("reveal-item--is-visible")
                elem.isRevealed = true
                if (elem.isLastItem) {
                    window.removeEventListener("scroll", this.scrollThrottle)
                }
            }
        }
    }

    hideInitially() {
        this.itemsToReveal.forEach(elem => {
            elem.classList.add("reveal-item")
            elem.isRevealed = false
        })
        this.itemsToReveal[this.itemsToReveal.length - 1].isLastItem = true
    }

}

export default RevealOnScroll;
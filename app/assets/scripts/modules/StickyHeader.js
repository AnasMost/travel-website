import throttle from "lodash/throttle";
import debounce from "lodash/debounce";

class StickyHeader {
    constructor() {
        this.siteHeader = document.querySelector(".site-header")
        this.pageSections = document.querySelectorAll(".page-section")
        this.browserHeight = window.innerHeight
        this.events()
    }

    events() {
        window.addEventListener("scroll", throttle(
            () => this.runOnScroll(), 200))
        window.addEventListener("resize", debounce(
            () => this.browserHeight = window.innerHeight, 444)
        )
    }

    runOnScroll() {
        if(window.scrollY > 60) {
            this.siteHeader.classList.add("site-header--dark")
        } else {
            this.siteHeader.classList.remove("site-header--dark")
        }

        this.pageSections.forEach(elem => {
            if(elem.offsetTop < window.scrollY + this.browserHeight && window.scrollY < elem.offsetTop + elem.offsetHeight) {
                let scrollRatioTop = elem.getBoundingClientRect().top / this.browserHeight
                let scrollRatioBottom = elem.getBoundingClientRect().bottom / this.browserHeight
                let matchingLink = elem.getAttribute("data-matching-link")
                if(scrollRatioTop < .18 && scrollRatioBottom > .33) {
                    document.querySelectorAll(`.primary-nav a:not(${matchingLink})`).forEach(item => 
                        item.classList.remove("is-current-link")
                    )
                    document.querySelector(matchingLink).classList.add("is-current-link")
                    this.activeLink = matchingLink
                } else if(scrollRatioTop > .35 && this.activeLink == matchingLink) {
                    document.querySelector(this.activeLink).classList.remove("is-current-link")
                }
            }
        })
    }
}

export default StickyHeader;